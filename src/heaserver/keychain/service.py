"""
The HEA Keychain manages credentials for connecting to networked services and resources.

AWS credentials endpoints provide functionality specific to AWS:
* Long-term credentials: An access key and secret that do not expire.  These credentials are typically owned by the 
user.
* Temporary credentials: Temporary credentials created using the user's HEA JWT token. The user is granted a short-term
access key and secret with a 12 hour lifespan that HEA refreshes automatically and in the background. Temporary
credentials are owned by the system|credentialsmanager user and shared with the requester. The requester is given 
VIEWER permissions.
* Secure managed credentials: starting from a user's temporary credentials, these endpoints create a permanent AWS
credential that is invalidated and deleted by this microservice after a defined lifespan. Secure managed credentials
are owned by the system|credentialsmanager user and shared with the requester. The requester is given VIEWER 
permissions. Secure managed credentials are created behind the scenes as part of presigned URL requests because 
otherwise the URLs would stop working as soon as the user's temporary credentials expire or are refreshed. They are
also created when a user requests a CLI credentials file, and the user can request a credentials lifespan of up to 72 
hours. Users can request to extend the lifespan by up to another 72 hours as many times as is needed.
"""
from datetime import timedelta
from functools import partial
from aiohttp import hdrs
from heaobject.activity import Status
from heaobject.data import ClipboardData
from heaobject.root import ShareImpl, Permission, ViewerPermissionContext, PermissionContext, DefaultPermissionGroup
from heaserver.service import response, appproperty, client
from heaserver.service.activity import DesktopObjectActionLifecycle
from heaserver.service.oidcclaimhdrs import SUB
from heaobject.user import NONE_USER, CREDENTIALS_MANAGER_USER
from heaserver.service.runner import init_cmd_line, routes, start, web, scheduled_cleanup_ctx
from heaserver.service.db import mongoservicelib, aws, mongo, awsservicelib
from heaserver.service.wstl import builder_factory, action
from heaserver.service.messagebroker import publisher_cleanup_context_factory, publish_desktop_object
from heaserver.service.util import now
from heaobject.keychain import Credentials, AWSCredentials, CredentialsView
from heaobject.awss3key import display_name
import asyncio
from heaserver.service.appproperty import HEA_DB
from botocore.exceptions import ClientError
from heaobject.error import DeserializeException
from collections.abc import Sequence, Collection
import logging

from mypy_boto3_iam import IAMClient
from mypy_boto3_iam.type_defs import ListAttachedRolePoliciesResponseTypeDef
from yarl import URL
from typing import Any, Literal, get_args, cast
import uuid
from email.utils import parsedate_to_datetime

_logger = logging.getLogger(__name__)
MONGODB_CREDENTIALS_COLLECTION = 'credentials'


@routes.get('/credentialsping')
async def ping(request: web.Request) -> web.Response:
    """
    Checks if this service is running.

    :param request: the HTTP request.
    :return: the HTTP response.
    """
    return await mongoservicelib.ping(request)


@routes.get('/credentials/{id}')
@action('heaserver-keychain-credentials-get-properties', rel='hea-properties hea-context-menu')
@action('heaserver-keychain-credentials-get-self', rel='self', path='credentials/{id}')
async def get_credentials(request: web.Request) -> web.Response:
    """
    Gets the credentials with the specified id.

    :param request: the HTTP request.
    :return: the requested credentials or Not Found.
    ---
    summary: A specific credentials.
    tags:
        - heaserver-keychain-get-credentials
    parameters:
        - $ref: '#/components/parameters/id'
    responses:
      '200':
        $ref: '#/components/responses/200'
      '404':
        $ref: '#/components/responses/404'
    """
    _logger.debug('Requested credentials by id %s' % request.match_info["id"])
    
    cred_dict = await mongoservicelib.get_dict(request, MONGODB_CREDENTIALS_COLLECTION)
    if cred_dict and cred_dict['type'] == Credentials.get_type_name():
        sub = request.headers.get(SUB, NONE_USER)
        context: PermissionContext[Credentials] = PermissionContext(sub)
        credentials = Credentials()
        credentials.from_dict(cred_dict)
        share = await credentials.get_permissions_as_share(context)
        credentials.shares = [share]
        attr_perms = await credentials.get_all_attribute_permissions(context)
        return await response.get(request, cred_dict, permissions=share.permissions, attribute_permissions=attr_perms)
    else:
        return response.status_not_found()


@routes.get('/awscredentials/{id}')
@action('heaserver-keychain-awscredentials-get-properties', rel='hea-properties hea-context-menu')
@action(name='heaserver-keychain-generate-awscredentials-file',
        rel='hea-dynamic-clipboard hea-generate-clipboard-icon hea-context-menu',
        path='awscredentials/{id}/climanagedcredentialscreator',
        itemif='temporary')
@action(name='heaserver-keychain-generate-awscredentials-file-managed',
        rel='hea-dynamic-clipboard hea-retrieve-clipboard-icon hea-context-menu',
        path='awscredentials/{id}/clicredentialsfile',
        itemif='not temporary and not for_presigned_url')
@action(name='heaserver-keychain-update-awscredentials-file',
        rel='hea-dynamic-standard hea-generate-clipboard-icon hea-context-menu',
        path='awscredentials/{id}/expirationextender',
        itemif='managed and not for_presigned_url')
@action('heaserver-keychain-credentials-get-self', rel='self', path='awscredentials/{id}')
async def get_aws_credentials(request: web.Request) -> web.Response:
    """
    Gets the AWS credentials with the specified id.

    :param request: the HTTP request.
    :return: the requested credentials or Not Found.
    ---
    summary: A specific credentials.
    tags:
        - heaserver-keychain-get-credentials
    parameters:
        - $ref: '#/components/parameters/id'
    responses:
      '200':
        $ref: '#/components/responses/200'
      '404':
        $ref: '#/components/responses/404'
    """
    _logger.debug('Requested AWS credentials by id %s' % request.match_info["id"])
    return await _get_aws_credentials(request)
    


@routes.get('/awscredentials/{id}/climanagedcredentialscreator')
@action(name="heaserver-keychain-generate-awscredentials-file-form")
async def get_aws_cli_credential_form(request: web.Request) -> web.Response:
    """
    Gets a form template for creating or extending a managed AWSCredentials object. Managed credentials have a defined 
    lifespan, and HEA deletes them after they have expired. Submitting this form will create a managed AWSCredentials
    object with the specified lifespan for the user and AWS account associated with the temporary AWSCredentials with 
    the given id. The HTTP request must either have no Accept header or an Accept header with a representor mimetype 
    that supports form templates.

    :param request: the HTTP request (required).
    :return: the requested form template.
    """
    return await _get_aws_credentials(request)

@routes.get('/awscredentials/{id}/clicredentialsfile')
@action(name="heaserver-keychain-generate-awscredentials-file-form-managed")
async def get_aws_cli_managed_credential_form(request: web.Request) -> web.Response:
    """
    Gets a form template for creating or extending a managed AWSCredentials object. Managed credentials have a defined 
    lifespan, and HEA deletes them after they have expired. Submitting this form will create a managed AWSCredentials
    object with the specified lifespan for the user and AWS account associated with the temporary AWSCredentials with 
    the given id. The HTTP request must either have no Accept header or an Accept header with a representor mimetype 
    that supports form templates.

    :param request: the HTTP request (required).
    :return: the requested form template.
    """
    return await _get_aws_credentials(request)


@routes.get('/awscredentials/{id}/expirationextender')
@action(name="heaserver-keychain-extend-expiration-form")
async def get_credentials_extender_form(request: web.Request) -> web.Response:
    """
    Gets a form template for extending the expiration of a managed AWSCredentials object. Submitting this form updates
    the expiration of the credentials. The HTTP request must either have no Accept header or an Accept header with a r
    epresentor mimetype that supports form templates.

    :param request: the HTTP request (required).
    :return: the requested form template.
    """
    return await _get_aws_credentials(request)


@routes.get('/credentials/byname/{name}')
async def get_credentials_by_name(request: web.Request) -> web.Response:
    """
    Gets the credentials with the specified name.

    :param request: the HTTP request.
    :return: the requested credentials or Not Found.
    ---
    summary: Specific credentials queried by name.
    tags:
        - heaserver-keychain-get-credentials-by-name
    parameters:
        - $ref: '#/components/parameters/name'
    responses:
      '200':
        $ref: '#/components/responses/200'
      '404':
        $ref: '#/components/responses/404'
    """
    _logger.debug('Requested credentials by name %s' % request.match_info["name"])
    sub = request.headers.get(SUB, NONE_USER)
    cred_dict = await mongoservicelib.get_by_name_dict(request, MONGODB_CREDENTIALS_COLLECTION)
    if cred_dict and cred_dict['type'] == Credentials.get_type_name():
        context: PermissionContext[Credentials] = PermissionContext(sub)
        credentials = Credentials()
        credentials.from_dict(cred_dict)
        share = await credentials.get_permissions_as_share(context)
        credentials.shares = [share]
        attr_perms = await credentials.get_all_attribute_permissions(context)
        return await response.get(request, cred_dict, permissions=share.permissions, attribute_permissions=attr_perms)
    else:
        return response.status_not_found()


@routes.get('/awscredentials/byname/{name}')
async def get_aws_credentials_by_name(request: web.Request) -> web.Response:
    """
    Gets the AWS credentials with the specified name.

    :param request: the HTTP request.
    :return: the requested credentials or Not Found.
    ---
    summary: Specific credentials queried by name.
    tags:
        - heaserver-keychain-get-credentials-by-name
    parameters:
        - $ref: '#/components/parameters/name'
    responses:
      '200':
        $ref: '#/components/responses/200'
      '404':
        $ref: '#/components/responses/404'
    """
    _logger.debug('Requested AWS credentials by name %s' % request.match_info["name"])
    sub = request.headers.get(SUB, NONE_USER)
    cred_dict = await mongoservicelib.get_by_name_dict(request, MONGODB_CREDENTIALS_COLLECTION)
    if cred_dict and cred_dict['type'] == AWSCredentials.get_type_name():
        context: PermissionContext[AWSCredentials] = PermissionContext(sub)
        aws_credentials = AWSCredentials()
        aws_credentials.from_dict(cred_dict)
        share = await aws_credentials.get_permissions_as_share(context)
        aws_credentials.shares = [share]
        attr_perms = await aws_credentials.get_all_attribute_permissions(context)
        return await response.get(request, cred_dict, permissions=share.permissions, attribute_permissions=attr_perms)
    else:
        return response.status_not_found()


@routes.get('/awscredentials')
@routes.get('/awscredentials/')
@action('heaserver-keychain-awscredentials-get-properties', rel='hea-properties hea-context-menu')
@action(name='heaserver-keychain-generate-awscredentials-file',
        rel='hea-dynamic-clipboard hea-generate-clipboard-icon hea-context-menu',
        path='awscredentials/{id}/climanagedcredentialscreator',
        itemif='temporary')
@action(name='heaserver-keychain-generate-awscredentials-file-managed',
        rel='hea-dynamic-clipboard hea-retrieve-clipboard-icon hea-context-menu',
        path='awscredentials/{id}/clicredentialsfile',
        itemif='not temporary and not for_presigned_url')
@action(name='heaserver-keychain-update-awscredentials-file',
        rel='hea-dynamic-standard hea-generate-clipboard-icon hea-context-menu',
        path='awscredentials/{id}/expirationextender',
        itemif='managed and not for_presigned_url')
@action('heaserver-keychain-credentials-get-self', rel='self', path='awscredentials/{id}')
async def get_all_aws_credentials(request: web.Request) -> web.Response:
    return await mongoservicelib.get_all(request, MONGODB_CREDENTIALS_COLLECTION, 
                                         mongoattributes={'type': AWSCredentials.get_type_name()})


@routes.get('/credentials')
@routes.get('/credentials/')
@action('heaserver-keychain-credentials-get-properties', rel='hea-properties hea-context-menu')
@action('heaserver-keychain-credentials-get-self', rel='self', path='credentials/{id}')
async def get_all_credentials(request: web.Request) -> web.Response:
    return await mongoservicelib.get_all(request, MONGODB_CREDENTIALS_COLLECTION, 
                                         mongoattributes={'type': Credentials.get_type_name()})


@routes.get('/credentialsviews')
@routes.get('/credentialsviews/')
@action('heaserver-keychain-credentialsviews-get-actual', rel='hea-actual', path='{+actual_object_uri}')
async def get_all_credentials_views(request: web.Request) -> web.Response:
    """
    Gets all credentials.

    :param request: the HTTP request.
    :return: all credentials.

    ---
    summary: All credentials.
    tags:
        - heaserver-keychain-get-all-credentials
    responses:
      '200':
        $ref: '#/components/responses/200'
    """
    sub = request.headers.get(SUB, NONE_USER)
    context: ViewerPermissionContext[CredentialsView] = ViewerPermissionContext(sub)
    views: list[CredentialsView] = []
    permissions: list[list[Permission]] = []
    attribute_permissions: list[dict[str, list[Permission]]] = []
    aws_credentials_type_name = AWSCredentials.get_type_name()
    credentials_type_name = Credentials.get_type_name()
    for credentials_dict in await mongoservicelib.get_all_dict(request, MONGODB_CREDENTIALS_COLLECTION):
        view = CredentialsView()
        id_ = credentials_dict['id']
        view.actual_object_id = id_
        view.actual_object_type_name = credentials_dict['type']
        if (display_name := credentials_dict.get('display_name')) is not None:
            view.display_name = display_name
        match credentials_dict_type_name := credentials_dict['type']:
            case aws_credentials_type_name if credentials_dict_type_name == aws_credentials_type_name:
                view.actual_object_uri = f'awscredentials/{id_}'
            case credentials_type_name if credentials_dict_type_name == credentials_type_name:
                view.actual_object_uri = f'credentials/{id_}'
            case _:
                raise ValueError(f'Unexpected desktop object type {credentials_dict_type_name}')
        view.id = f'{credentials_dict_type_name}^{id_}'
        share, attr_perms = await asyncio.gather(view.get_permissions_as_share(context),
                                                 view.get_all_attribute_permissions(context))
        view.shares = [share]
        views.append(view)
        permissions.append(share.permissions)
        attribute_permissions.append(attr_perms)
    view_dicts = [v.to_dict() for v in views]
    return await response.get_all(request, view_dicts, permissions, attribute_permissions)


@routes.post('/credentials')
@routes.post('/credentials/')
async def post_credentials(request: web.Request) -> web.Response:
    """
    Posts the provided credentials.

    :param request: the HTTP request.
    :return: a Response object with a status of Created and the object's URI in the Location header.
    ---
    summary: Credentials creation
    tags:
        - heaserver-keychain-post-credentials
    requestBody:
      description: A new credentials object.
      required: true
      content:
        application/vnd.collection+json:
          schema:
            type: object
          examples:
            example:
              summary: Credentials
              value: {
                "template": {
                  "data": [
                    {
                      "name": "created",
                      "value": null,
                      "prompt": "created",
                      "display": true
                    },
                    {
                      "name": "derived_by",
                      "value": null,
                      "prompt": "derived_by",
                      "display": true
                    },
                    {
                      "name": "derived_from",
                      "value": [],
                      "prompt": "derived_from",
                      "display": true
                    },
                    {
                      "name": "description",
                      "value": null,
                      "prompt": "description",
                      "display": true
                    },
                    {
                      "name": "display_name",
                      "value": "Joe",
                      "prompt": "display_name",
                      "display": true
                    },
                    {
                      "name": "invites",
                      "value": [],
                      "prompt": "invites",
                      "display": true
                    },
                    {
                      "name": "modified",
                      "value": null,
                      "prompt": "modified",
                      "display": true
                    },
                    {
                      "name": "name",
                      "value": "joe",
                      "prompt": "name",
                      "display": true
                    },
                    {
                      "name": "owner",
                      "value": "system|none",
                      "prompt": "owner",
                      "display": true
                    },
                    {
                      "name": "shares",
                      "value": [],
                      "prompt": "shares",
                      "display": true
                    },
                    {
                      "name": "source",
                      "value": null,
                      "prompt": "source",
                      "display": true
                    },
                    {
                      "name": "version",
                      "value": null,
                      "prompt": "version",
                      "display": true
                    },
                    {
                      "name": "type",
                      "value": "heaobject.keychain.Credentials"
                    }
                  ]
                }
              }
        application/json:
          schema:
            type: object
          examples:
            example:
              summary: Credentials
              value: {
                "created": null,
                "derived_by": null,
                "derived_from": [],
                "description": null,
                "display_name": "Joe",
                "invites": [],
                "modified": null,
                "name": "joe",
                "owner": "system|none",
                "shares": [],
                "source": null,
                "type": "heaobject.keychain.Credentials",
                "version": null
              }
    responses:
      '201':
        $ref: '#/components/responses/201'
      '400':
        $ref: '#/components/responses/400'
      '404':
        $ref: '#/components/responses/404'
    """
    return await mongoservicelib.post(request, MONGODB_CREDENTIALS_COLLECTION, Credentials)


@routes.post('/awscredentials')
@routes.post('/awscredentials/')
async def post_aws_credentials(request: web.Request) -> web.Response:
    """
    Posts the provided AWS credentials.

    :param request: the HTTP request.
    :return: a Response object with a status of Created and the object's URI in the Location header.
    ---
    summary: Credentials creation
    tags:
        - heaserver-keychain-post-credentials
    requestBody:
      description: A new credentials object.
      required: true
      content:
        application/vnd.collection+json:
          schema:
            type: object
          examples:
            example:
              summary: Credentials
              value: {
                "template": {
                  "data": [
                    {
                      "name": "created",
                      "value": null,
                      "prompt": "created",
                      "display": true
                    },
                    {
                      "name": "derived_by",
                      "value": null,
                      "prompt": "derived_by",
                      "display": true
                    },
                    {
                      "name": "derived_from",
                      "value": [],
                      "prompt": "derived_from",
                      "display": true
                    },
                    {
                      "name": "description",
                      "value": null,
                      "prompt": "description",
                      "display": true
                    },
                    {
                      "name": "display_name",
                      "value": "Joe",
                      "prompt": "display_name",
                      "display": true
                    },
                    {
                      "name": "invites",
                      "value": [],
                      "prompt": "invites",
                      "display": true
                    },
                    {
                      "name": "modified",
                      "value": null,
                      "prompt": "modified",
                      "display": true
                    },
                    {
                      "name": "name",
                      "value": "joe",
                      "prompt": "name",
                      "display": true
                    },
                    {
                      "name": "owner",
                      "value": "system|none",
                      "prompt": "owner",
                      "display": true
                    },
                    {
                      "name": "shares",
                      "value": [],
                      "prompt": "shares",
                      "display": true
                    },
                    {
                      "name": "source",
                      "value": null,
                      "prompt": "source",
                      "display": true
                    },
                    {
                      "name": "version",
                      "value": null,
                      "prompt": "version",
                      "display": true
                    },
                    {
                      "name": "type",
                      "value": "heaobject.keychain.AWSCredentials"
                    }
                  ]
                }
              }
        application/json:
          schema:
            type: object
          examples:
            example:
              summary: Credentials
              value: {
                "created": null,
                "derived_by": null,
                "derived_from": [],
                "description": null,
                "display_name": "Joe",
                "invites": [],
                "modified": null,
                "name": "joe",
                "owner": "system|none",
                "shares": [],
                "source": null,
                "type": "heaobject.keychain.AWSCredentials",
                "version": null
              }
    responses:
      '201':
        $ref: '#/components/responses/201'
      '400':
        $ref: '#/components/responses/400'
      '404':
        $ref: '#/components/responses/404'
    """
    # If managed credentials are being created, then create the credentials in AWS first.
    return await mongoservicelib.post(request, MONGODB_CREDENTIALS_COLLECTION, AWSCredentials, resource_base='awscredentials')


_UniqueAttributes = Literal['OIDC_CLAIM_sub', 'account_id', 'key_lifespan']


@routes.post('/awscredentials/{id}/expirationextender')
async def post_credentials_extender_form(request: web.Request) -> web.Response:
    aws_cred = await _get_aws_cred(request)
    if aws_cred is None:
        return response.status_not_found("Could not get credential")
    if not aws_cred.managed:
        return response.status_bad_request('Cannot extend expiration of non-managed credentials')
    else:
        async with mongo.MongoContext(request) as mongo_client:
            existing_creds = aws_cred
            existing_creds.extend()
            existing_creds.modified = now()
            result = await mongo_client.update_admin(existing_creds, MONGODB_CREDENTIALS_COLLECTION)
            if result is not None and result.matched_count:
                to_delete = []
                for cache_key in request.app[appproperty.HEA_CACHE]:
                    if cache_key[1] == MONGODB_CREDENTIALS_COLLECTION and cache_key[2] in (None, f"id^{request.match_info['id']}"):
                        to_delete.append(cache_key)
                for cache_key in to_delete:
                    request.app[appproperty.HEA_CACHE].pop(cache_key, None)
            return response.status_no_content()


@routes.post('/awscredentials/internal/{id}/presignedurlcredentialscreator')
async def post_presigned_url_credentials_creator(request: web.Request) -> web.Response:
    """
    Posts a template for requesting the generation of managed credentials for a collection of presigned URLs. Returns
    the created credentials' URL in the response's Location header.

    :param request: the HTTP request.
    :return: No Content or Not Found.
    ---
    summary: Managed credentials url
    tags:
        - heaserver-keychain
    parameters:
        - $ref: '#/components/parameters/id'
        - name: Authorization
          type: header
          required: true
          description: The Authorization header value.
          schema:
            type: string
    requestBody:
        description: The expiration time for the presigned URL.
        required: true
        content:
            application/vnd.collection+json:
              schema:
                type: object
              examples:
                example:
                  summary: The time before the key expires in hours
                  value:
                    "template": {
                      "data": [
                      {
                        "name": "key_lifespan",
                        "value": 24
                      },{
                        "name": "keys",
                        "value": [
                          'foo/bar/baz',
                          'foo/bar/baz2'
                        ]
                      }]
                    }
            application/json:
              schema:
                type: object
    """
    credential_id = request.match_info['id']
    # request for admin
    auth_header_value = request.headers.get(hdrs.AUTHORIZATION)
    if auth_header_value is None:
        return response.status_bad_request('No Authorization header value')
    req = request.clone(headers={hdrs.CONTENT_TYPE: 'application/json',
                                 SUB: CREDENTIALS_MANAGER_USER,
                                 hdrs.AUTHORIZATION: auth_header_value
                                 })
    try:
        attr_values = await _extract_attribute_values(await request.json())
    except Exception as e:
        return response.status_bad_request(body=f"Invalid template: {e}")
    try:
        aws_cred = await _get_aws_cred(request)
    except Exception as e:
        aws_cred_exception: Exception | None = e
    else:
        aws_cred_exception = None
    async with DesktopObjectActionLifecycle(request=request,
                                            code='hea-create',
                                            description=f'Creating AWS presigned URL credentials from {aws_cred if aws_cred is not None else credential_id}',
                                            activity_cb=publish_desktop_object) as activity:
        if aws_cred is None:
            raise aws_cred_exception if aws_cred_exception is not None else response.status_not_found()
        if aws_cred is not None and aws_cred.role is None:
            raise response.status_bad_request('Cannot create managed credentials from these credentials: No role is defined')
        admin_cred = await request.app[HEA_DB].elevate_privileges(request, aws_cred)
        async with aws.IAMClientContext(request=req, credentials=admin_cred) as iam_admin_client:
            try:
                aws_cred = await _get_presigned_url_user_credentials(request, aws_cred, iam_admin_client, 
                                                                     key_lifespan=attr_values['key_lifespan'],
                                                                     keys=attr_values['keys'])
                _logger.debug("aws_cred ready to post: %r", aws_cred)
            except KeyError as ke:
                raise response.status_bad_request(f"Managed credentials were not created: {ke}") from ke
            except _BadRequestException as e:
                raise response.status_bad_request(f"Managed credentials were not created: {e}") from e
        return await response.post(request, aws_cred.id, 'awscredentials')


@routes.post('/awscredentials/{id}/climanagedcredentialscreator')
async def post_aws_credentials_form(request: web.Request) -> web.Response:
    """
    Posts a template for requesting the generation of managed credentials, and returns the managed credentials as
    a ClipboardData object. The request will fail if the given credentials are not temporary.

    :param request: the HTTP request.
    :return: No Content or Not Found.
    ---
    summary: Managed credentials url
    tags:
        - heaserver-keychain
    parameters:
        - name: id
          in: path
          required: true
          description: The id of the credentials.
          schema:
            type: string
          examples:
            example:
              summary: A credential id
              value: 666f6f2d6261722d71757578
        - $ref: '#/components/parameters/id'
        - name uniqueattribute
          in: query
          required: false
          description: Fail if the specified form template attributes are not unique. The sum of the lengths of the 
          attribute values may not be longer than 58 characters.
          schema:
            type: array
          examples:
            example:
              summary: A unique attribute
              value: 
    requestBody:
        description: The expiration time for the presigned URL.
        required: true
        content:
            application/vnd.collection+json:
              schema:
                type: object
              examples:
                example:
                  summary: The time before the key expires in hours
                  value: {
                    "template": {
                      "data": [
                      {
                        "name": "key_lifespan",
                        "value": 72
                      }]
                    }
                  }
            application/json:
              schema:
                type: object
              examples:
                example:
                  summary: The time before the key expires in hours
                  value: {
                    "key_lifespan": 72
                  }
    responses:
      '200':
        $ref: '#/components/responses/200'
      '403':
        $ref: '#/components/responses/403'
      '404':
        $ref: '#/components/responses/404'
    """
    credential_id = request.match_info.get('id', None)
    uniqueattributes: dict[Literal['OIDC_CLAIM_sub', 'account_id', 'key_lifespan'], None] = {'OIDC_CLAIM_sub': None, 'account_id': None, 'key_lifespan': None}
    # request for admin
    auth_header_value = request.headers.get(hdrs.AUTHORIZATION)
    if auth_header_value is None:
        return response.status_bad_request('No Authorization header value')
    req = request.clone(headers={hdrs.CONTENT_TYPE: 'application/json',
                                 SUB: CREDENTIALS_MANAGER_USER,
                                 hdrs.AUTHORIZATION: auth_header_value
                                 })
    if not credential_id:
        return response.status_bad_request(body="credential id is required")

    try:
        attr_values = await _extract_attribute_values(await request.json())
    except Exception as e:
        return response.status_bad_request(body=f"Invalid template: {e}")
    try:
        aws_cred = await _get_aws_cred(request)
    except Exception as e:
        aws_cred_exception: Exception | None = e
    else:
        aws_cred_exception = None

    async with DesktopObjectActionLifecycle(request=request,
                                            code='hea-create',
                                            description=f'Creating Managed AWS CLI credentials from {aws_cred.display_name if aws_cred is not None else credential_id}',
                                            activity_cb=publish_desktop_object) as activity:
        if aws_cred is None:
            raise aws_cred_exception if aws_cred_exception is not None else response.status_not_found()
        if aws_cred is not None:
            if aws_cred.role is None:
                raise response.status_bad_request('Cannot create managed credentials from these credentials: No role is defined')
            if not aws_cred.temporary:
                raise response.status_bad_request('Cannot create managed credentials from these credentials: They are not temporary')
        admin_cred = await request.app[HEA_DB].elevate_privileges(request, aws_cred)
        async with aws.IAMClientContext(request=req, credentials=admin_cred) as iam_admin_client:
            try:
                aws_cred = await _get_managed_user_credentials(request, aws_cred, iam_admin_client, 
                                                               uniqueattributes, **attr_values)
                _logger.debug("aws_cred ready to post: %r", aws_cred)
            except _BadRequestException as e:
                raise response.status_bad_request(f"Managed credentials were not created: {e}") from e
        data = ClipboardData()
        data.mime_type = 'text/plain;encoding=utf-8'
        data.created = now()
        data.display_name = f'AWS CLI credentials file for {aws_cred.display_name}'
        data.data = aws_cred.to_credentials_file_str()
        return await response.get(request, data.to_dict())


@routes.post('/awscredentials/{id}/clicredentialsfile')
async def post_aws_managed_credentials_form(request: web.Request) -> web.Response:
    """
    Posts a template for requesting previously generated credentials in file format, returned as a ClipboardData 
    object.

    :param request: the HTTP request.
    :return: 200 status code and the ClipboardData object in the response body.
    ---
    summary: Managed credential url
    tags:
        - heaserver-keychain
    parameters:
        - name: id
          in: path
          required: true
          description: The id of the credential.
          schema:
            type: string
          examples:
            example:
              summary: A credential id
              value: 666f6f2d6261722d71757578
        - $ref: '#/components/parameters/id'
    responses:
      '200':
        $ref: '#/components/responses/200'
      '403':
        $ref: '#/components/responses/403'
      '404':
        $ref: '#/components/responses/404'
    """
    aws_cred = await _get_aws_cred(request)
    if aws_cred is None:
        return response.status_not_found("Could not get credentials")
    data = ClipboardData()
    data.mime_type = 'text/plain;encoding=utf-8'
    data.created = now()
    data.display_name = f'AWS CLI credentials file for {aws_cred.display_name}'
    data.data = aws_cred.to_credentials_file_str()
    return await response.get(request, data.to_dict())


@routes.put('/credentials/{id}')
async def put_credentials(request: web.Request) -> web.Response:
    """
    Updates the credentials with the specified id.
    :param request: the HTTP request.
    :return: a Response object with a status of No Content or Not Found.
    ---
    summary: Credentials updates
    tags:
        - heaserver-keychain-put-credentials
    parameters:
        - $ref: '#/components/parameters/id'
    requestBody:
      description: An updated credentials object.
      required: true
      content:
        application/vnd.collection+json:
          schema:
            type: object
          examples:
            example:
              summary: Credentials
              value: {
                "template": {
                  "data": [
                    {
                      "name": "created",
                      "value": null
                    },
                    {
                      "name": "derived_by",
                      "value": null
                    },
                    {
                      "name": "derived_from",
                      "value": []
                    },
                    {
                      "name": "name",
                      "value": "reximus"
                    },
                    {
                      "name": "description",
                      "value": null
                    },
                    {
                      "name": "display_name",
                      "value": "Reximus Max"
                    },
                    {
                      "name": "invites",
                      "value": []
                    },
                    {
                      "name": "modified",
                      "value": null
                    },
                    {
                      "name": "owner",
                      "value": "system|none"
                    },
                    {
                      "name": "shares",
                      "value": []
                    },
                    {
                      "name": "source",
                      "value": null
                    },
                    {
                      "name": "version",
                      "value": null
                    },
                    {
                      "name": "id",
                      "value": "666f6f2d6261722d71757578"
                    },
                    {
                      "name": "type",
                      "value": "heaobject.keychain.Credentials"
                    }
                  ]
                }
              }
        application/json:
          schema:
            type: object
          examples:
            example:
              summary: An updated credentials object
              value: {
                "created": None,
                "derived_by": None,
                "derived_from": [],
                "name": "reximus",
                "description": None,
                "display_name": "Reximus Max",
                "invites": [],
                "modified": None,
                "owner": NONE_USER,
                "shares": [],
                "source": None,
                "type": "heaobject.keychain.Credentials",
                "version": None,
                "id": "666f6f2d6261722d71757578"
              }
    responses:
      '204':
        $ref: '#/components/responses/204'
      '400':
        $ref: '#/components/responses/400'
      '404':
        $ref: '#/components/responses/404'
    """
    return await mongoservicelib.put(request, MONGODB_CREDENTIALS_COLLECTION, Credentials)


@routes.put('/awscredentials/{id}')
async def put_aws_credentials(request: web.Request) -> web.Response:
    """
    Updates the credentials with the specified id.
    :param request: the HTTP request.
    :return: a Response object with a status of No Content or Not Found.
    ---
    summary: Credentials updates
    tags:
        - heaserver-keychain-put-credentials
    parameters:
        - $ref: '#/components/parameters/id'
    requestBody:
      description: An updated credentials object.
      required: true
      content:
        application/vnd.collection+json:
          schema:
            type: object
          examples:
            example:
              summary: Credentials
              value: {
                "template": {
                  "data": [
                    {
                      "name": "created",
                      "value": null
                    },
                    {
                      "name": "derived_by",
                      "value": null
                    },
                    {
                      "name": "derived_from",
                      "value": []
                    },
                    {
                      "name": "name",
                      "value": "reximus"
                    },
                    {
                      "name": "description",
                      "value": null
                    },
                    {
                      "name": "display_name",
                      "value": "Reximus Max"
                    },
                    {
                      "name": "invites",
                      "value": []
                    },
                    {
                      "name": "modified",
                      "value": null
                    },
                    {
                      "name": "owner",
                      "value": "system|none"
                    },
                    {
                      "name": "shares",
                      "value": []
                    },
                    {
                      "name": "source",
                      "value": null
                    },
                    {
                      "name": "version",
                      "value": null
                    },
                    {
                      "name": "id",
                      "value": "666f6f2d6261722d71757578"
                    },
                    {
                      "name": "type",
                      "value": "heaobject.keychain.AWSCredentials"
                    }
                  ]
                }
              }
        application/json:
          schema:
            type: object
          examples:
            example:
              summary: An updated credentials object
              value: {
                "created": None,
                "derived_by": None,
                "derived_from": [],
                "name": "reximus",
                "description": None,
                "display_name": "Reximus Max",
                "invites": [],
                "modified": None,
                "owner": NONE_USER,
                "shares": [],
                "source": None,
                "type": "heaobject.keychain.AWSCredentials",
                "version": None,
                "id": "666f6f2d6261722d71757578"
              }
    responses:
      '204':
        $ref: '#/components/responses/204'
      '400':
        $ref: '#/components/responses/400'
      '404':
        $ref: '#/components/responses/404'
    """
    # If managed credentials are being updated, we need to check if the expiration datetime is being changed. If so, then
    # update the managed credentials on AWS with the new expiration datetime.
    async def _update_managed_aws_credentials(request: web.Request, aws_cred: AWSCredentials):
        old_aws_cred = await mongoservicelib.get_desktop_object(request, MONGODB_CREDENTIALS_COLLECTION, type_=AWSCredentials)
        if old_aws_cred is not None:
            if aws_cred.managed and not old_aws_cred.managed:  # Newly managed, so create the AWS user/creds.
                pass
            elif old_aws_cred.managed and not aws_cred.managed:  # Not managed anymore, so delete the AWS user/creds.
                pass
    return await mongoservicelib.put(request, MONGODB_CREDENTIALS_COLLECTION, AWSCredentials,
                                     pre_save_hook=_update_managed_aws_credentials)


@routes.delete('/credentials/{id}')
async def delete_credentials(request: web.Request) -> web.Response:
    """
    Deletes the credentials with the specified id.
    
    :param request: the HTTP request.
    :return: No Content or Not Found.
    ---
    summary: Credentials deletion
    tags:
        - heaserver-keychain-delete-credentials
    parameters:
        - $ref: '#/components/parameters/id'
    responses:
      '204':
        $ref: '#/components/responses/204'
      '404':
        $ref: '#/components/responses/404'
    """
    cred = await _get_cred(request)
    id_ = request.match_info['id']
    async with DesktopObjectActionLifecycle(request=request,
                                            code='hea-delete',
                                            description=f"Deleting credential {cred.display_name if cred else 'with id ' + id_}",
                                            activity_cb=publish_desktop_object) as activity:
        activity.old_object_id = id_
        activity.old_object_type_name = Credentials.get_type_name()
        activity.old_object_uri = f'credentials/{id_}'
        if cred is None:
            raise response.status_not_found()
        resp = await mongoservicelib.delete(request, MONGODB_CREDENTIALS_COLLECTION)  # we do this first to make sure the user has delete permissions.
        if resp.status != 204:
            activity.status = Status.FAILED
        return resp


@routes.delete('/awscredentials/{id}')
async def delete_awscredentials(request: web.Request) -> web.Response:
    """
    Deletes the AWS credentials with the specified id, as well as any managed credentials that were created with it.
    
    :param request: the HTTP request.
    :return: No Content or Not Found.
    ---
    summary: AWS Credentials deletion
    tags:
        - heaserver-keychain-delete-awscredentials
    parameters:
        - $ref: '#/components/parameters/id'
    responses:
      '204':
        $ref: '#/components/responses/204'
      '404':
        $ref: '#/components/responses/404'
    """
    id_ = request.match_info['id']
    aws_cred = await _get_aws_cred(request)
    async with DesktopObjectActionLifecycle(request=request,
                                            code='hea-delete',
                                            description=f"Deleting AWS credential {aws_cred.display_name if aws_cred else 'with id ' + id_}",
                                            activity_cb=publish_desktop_object) as activity:
        activity.old_object_id = id_
        activity.old_object_type_name = AWSCredentials.get_type_name()
        activity.old_object_uri = f'awscredentials/{id_}'

        if aws_cred is None:
            raise response.status_not_found()
        resp = await mongoservicelib.delete(request, MONGODB_CREDENTIALS_COLLECTION)# we do this first to make sure the user has delete permissions.
        if resp.status == 204 and aws_cred.managed:
            try:
                await _cleanup_deleted_managed_aws_credential(request, aws_cred)
            except (ValueError, ClientError) as e:
                raise response.status_bad_request(f"Unable to delete AWS managed credentials: {e}")
        if resp.status != 204:
            activity.status = Status.FAILED
        return resp


async def _cleanup_deleted_managed_aws_credential(request: web.Request, aws_cred: AWSCredentials):
    """
    Deletes all the managed credentials that were generated using the AWS credentials with the given ID. The passed in
    credentials must have a non-None role. This function involves privilege elevation. This function attempts to clean
    up any partial user and access key information that may have been created in AWS IAM.

    :param request: the HTTP request (required).
    :param aws_cred: a managed AWS credentials object (required).
    :raises ValueError: if an error occurred getting elevated privileges. 
    :raises ClientError: if an error occurred contacting AWS. NoSuchEntity errors are not raised, as they may occur 
    when the information to delete has been previously deleted or is otherwise missing.
    """
    loop = asyncio.get_running_loop()
    assert aws_cred.aws_role_name is not None, 'aws_cred cannot have a None role'
    admin_cred = await request.app[HEA_DB].elevate_privileges(request, aws_cred)
    async with aws.IAMClientContext(request=request, credentials=admin_cred) as iam_admin_client:
        async with _managed_credentials_lock:
            try:
                r_policies = await loop.run_in_executor(None,
                                                        partial(iam_admin_client.list_attached_role_policies,
                                                                RoleName=aws_cred.aws_role_name))
                assert aws_cred.name is not None, 'aws_cred.name cannot be None'
                await _delete_managed_user(iam_client=iam_admin_client, username=aws_cred.name, policies=r_policies)
            except ClientError as e:
                if aws.client_error_code(e) != aws.CLIENT_ERROR_NO_SUCH_ENTITY:
                    raise e


async def _create_or_get_managed_user(iam_client: IAMClient, username: str,
                                policies: ListAttachedRolePoliciesResponseTypeDef):
    """
    Creates a managed user in AWS IAM with the attached user policies, and creates an access key.
    
    :param iam_client: the IAM client.
    :param username: the username of the managed user. Must be 20-64 characters long.
    :param policies: the policies attached to the managed user.
    :return: the access key response.
    :raises ClientError: if an error occurred creating the user, attaching the policies, or creating an access key.
    :raise ValueError: if the user already exists but has no access key.
    """
    while True:
        try:
            iam_client.create_user(UserName=username)
            break
        except ClientError as e:
            if aws.client_error_code(e) == 'EntityAlreadyExists':
                await _delete_managed_user(iam_client, username, policies)
            else:
                raise e
    await asyncio.sleep(0)
    for policy in policies['AttachedPolicies']:
        iam_client.attach_user_policy(UserName=username, PolicyArn=policy['PolicyArn'])
        await asyncio.sleep(0)
    return iam_client.create_access_key(UserName=username)['AccessKey']

async def _delete_managed_user(iam_client: IAMClient, username: str,
                               policies: ListAttachedRolePoliciesResponseTypeDef | None = None):
    """
    Deletes the managed user in AWS IAM.
    
    :param iam_client: the IAM client.
    :param username: the username of the managed user. Must be 20-64 characters long.
    :param policies: the policies attached to the managed user.
    :return: the response from the delete user operation.
    """
    if policies:
        for policy in policies['AttachedPolicies']:
            iam_client.detach_user_policy(UserName=username, PolicyArn=policy['PolicyArn'])
            await asyncio.sleep(0)
    access_keys = iam_client.list_access_keys(UserName=username)['AccessKeyMetadata']
    await asyncio.sleep(0)
    for metadata in access_keys:
        iam_client.delete_access_key(UserName=username, AccessKeyId=metadata['AccessKeyId'])
        await asyncio.sleep(0)
    iam_client.delete_user(UserName=username)


_managed_credentials_lock = asyncio.Lock()

async def _delete_managed_coro(app: web.Application):
    session = app[appproperty.HEA_CLIENT_SESSION]
    if not session:
        _logger.debug("session does not exist ")
        return

    try:
        headers_ = {SUB: CREDENTIALS_MANAGER_USER}
        component = await client.get_component_by_name(app, 'heaserver-keychain', client_session=session)
        assert component is not None, 'registry entry for heaserver-keychain not found'
        assert component.base_url is not None, 'registry entry for heaserver-keychain has no base_url'
        async with _managed_credentials_lock:
            coros_to_gather = []
            async for cred in client.get_all(app=app, url=URL(component.base_url) / 'awscredentials',
                                            type_=AWSCredentials, headers=headers_):
                if cred.managed and cred.has_expired():
                    assert cred.id is not None, 'exp_cred.id cannot be None'
                    _logger
                    coros_to_gather.append(client.delete(app, URL(component.base_url) / 'awscredentials' / cred.id, headers_))
            await asyncio.gather(*coros_to_gather)
    except Exception as ex:
        _logger.debug("an exception occurred", exc_info=ex)


async def _get_aws_cred(request: web.Request) -> AWSCredentials | None:
    """
    Retrieves the AWS credentials from the database, returning None if an error occurred or the credentials could not 
    be found."""
    aws_cred = AWSCredentials()
    try:
        cred_dict = await mongoservicelib.get_dict(request, MONGODB_CREDENTIALS_COLLECTION)
        if cred_dict is None:
            raise Exception("Could not get credential")
        aws_cred.from_dict(cred_dict)
    except (DeserializeException, Exception) as e:
        return None
    return aws_cred


async def _get_cred(request: web.Request) -> Credentials | None:
    cred = Credentials()
    try:
        cred_dict = await mongoservicelib.get_dict(request, MONGODB_CREDENTIALS_COLLECTION)
        if cred_dict is None:
            raise Exception("Could not get credential")
        cred.from_dict(cred_dict)
    except (DeserializeException, Exception) as e:
        return None
    return cred


async def _extract_attribute_values(body: dict[str, Any]) -> dict[str, Any]:
    """
    Extracts the target URL and expiration time in hours for a presigned URL request. It un-escapes them as needed.

    :param body: a Collection+JSON template dict.
    :return: a three-tuple containing the target URL and the un-escaped expiration time in hours.
    :raises web.HTTPBadRequest: if the given body is invalid.
    """
    try:
        return {item['name']: item['value'] for item in body['template']['data']}
    except (KeyError, ValueError) as e:
        raise web.HTTPBadRequest(body=f'Invalid template: {e}') from e


async def _get_managed_user_credentials(request: web.Request, aws_cred: AWSCredentials, iam_admin_client: IAMClient,
                                       unique_attributes: dict[_UniqueAttributes, None] | None = None,
                                       OIDC_CLAIM_sub: str | None = None, 
                                       account_id: str | None = None, key_lifespan: str = '12') -> AWSCredentials:
    """
    Gets managed user credentials for the given user for use with AWS CLI, creating the credentials if necessary. The 
    name of the credentials is constructed from the sub, the given credentials' account id, and the given unique_id. If 
    the unique_id is None, a UUID will be generated for the unique value.

    :param request: the HTTP request.
    :param aws_cred: the user's credentials.
    :param iam_admin_client: an IAM client with elevated privileges.
    :param unique_attributes: which subset of sub, account_id, and key_lifespan to use for checking for managed
    credentials uniqueness. If empty, no uniqueness check is performed. If populated, this function will raise
    _BadRequestException if a managed credentials object with the same unique values already exists.
    :return: the managed user credentials.
    :raises _BadRequestException: if an error occurred creating the managed user credentials due to user input.
    """
    parameters = {k: v for k, v in locals().items() if k in unique_attributes.keys()} if unique_attributes is not None else None
    sub_from_request = request.headers.get(SUB, NONE_USER)
    key_lifespan_ = int(key_lifespan)
    if key_lifespan_ not in (12, 24, 36, 48, 72):
        raise _BadRequestException('Key lifespan must be one of 12, 24, 36, 48, or 72')
    
    if not parameters:
        unique_value_ = f'auto_{uuid.uuid1()}'
    else:
        if OIDC_CLAIM_sub is not None and OIDC_CLAIM_sub != sub_from_request:
            raise _BadRequestException('sub must be the requesting username')
        unique_value_ = 'man_' + '_'.join(parameters.values())
        # unique_value_ can be max 64 characters long because AWS usernames can be max 64 characters long.
        assert len(unique_value_) <= 64, 'The sum of the lengths of the unique attribute values is too long'
    return await _get_managed_user_credentials_0(request, sub_from_request, key_lifespan_, aws_cred, iam_admin_client, unique_value_,
                                                  f"{aws_cred.display_name} Managed {key_lifespan}hr")


async def _get_presigned_url_user_credentials(request: web.Request, aws_cred: AWSCredentials, 
                                              iam_admin_client: IAMClient, key_lifespan: str = '24',
                                              keys: Collection[str] | None = None) -> AWSCredentials:
    """
    Gets managed user credentials for the given user for use with AWS CLI, creating the credentials if necessary. The 
    name of the credentials is constructed from the sub, the given credentials' account id, and the given unique_id. If 
    the unique_id is None, a UUID will be generated for the unique value.

    :param request: the HTTP request.
    :param aws_cred: the user's credentials.
    :param iam_admin_client: an IAM client with elevated privileges.
    :param key_lifespan: the requested lifespan in hours (up to one week). Default is 24 hours.
    :param keys: the keys of the S3 objects in the presigned URL request. Used for setting the credentials' display 
    name.
    :return: the managed user credentials.
    :raises _BadRequestException: if an error occurred creating the user credentials due to user input.
    """
    sub = request.headers.get(SUB, NONE_USER)
    key_lifespan_ = int(key_lifespan)
    if key_lifespan_ not in (24, 72, 168):
        raise _BadRequestException('Key lifespan must be one of 24, 72, 168')
    if keys is None:
        keys_str = ''
    elif len(keys) == 1:
        keys_str = f' for {display_name(next(iter(keys)))}'
    else:
        keys_str = f's for {display_name(next(iter(keys)))} and More'
    return await _get_managed_user_credentials_0(request, sub, key_lifespan_, aws_cred, iam_admin_client, 
                                                 f'presignedurl_{uuid.uuid1()}', 
                                                 f"{aws_cred.display_name} Presigned URL{keys_str} {key_lifespan_}hr")


async def _get_managed_user_credentials_0(request: web.Request, sub: str, key_lifespan: int, aws_cred: AWSCredentials, 
                                          iam_admin_client: IAMClient, name: str, display_name: str) -> AWSCredentials:
    """
    Gets managed user credentials for the given user, creating credentials if necessary. If you supply managed
    credentials in the aws_cred argument, this function returns a copy of the credentials back. If you supply temporary
    credentials, this function will create managed credentials from them. If you supply non-managed, non-temporary
    credentials, this function will raise an error.

    :param request: the HTTP request.
    :param sub: the requesting username.
    :param key_lifespan: the requested lifespan in hours (up to 72).
    :param aws_cred: the user's credentials.
    :param iam_admin_client: an IAM client with elevated privileges.
    :param name: the name to use for the managed credentials. Max 64 characters long.
    :param display_name: the display name to use for the managed credentials.
    :return: the managed user credentials, with its id set from when it was persisted.
    :raises _BadRequestException: if an error occurred creating the managed user credentials due to user input.
    """
    if len(name) > 64:
        raise ValueError('name is too long; can be at most 64 characters in length')
    if not aws_cred.temporary:
        raise _BadRequestException('These credentials are not temporary')
    async with mongo.MongoContext(request) as mongo_client:
        async with _managed_credentials_lock:
            aws_cred_ = AWSCredentials()
            try:
                role_name = aws_cred.aws_role_name
                if not role_name:
                    raise _BadRequestException('No role is defined')
                loop = asyncio.get_running_loop()
                aws_cred_old = await mongo_client.get(request, MONGODB_CREDENTIALS_COLLECTION, 
                                                      mongoattributes={'name': name}, sub=sub)
                if aws_cred_old is None:
                    r_policies = await loop.run_in_executor(None, partial(iam_admin_client.list_attached_role_policies, 
                                                                        RoleName=role_name))

                    access_key = await _create_or_get_managed_user(iam_client=iam_admin_client, username=name, 
                                                                   policies=r_policies)
                    _logger.debug("Creating credentials on AWS for %s with role %s ", name, role_name)
                    aws_cred_.account = access_key['AccessKeyId']
                    aws_cred_.password = access_key['SecretAccessKey']
                    aws_cred_.created = access_key['CreateDate']
                    aws_cred_.modified = access_key['CreateDate']
                    aws_cred_.session_token = None
                    aws_cred_.temporary = False
                    aws_cred_.managed = True
                    aws_cred_.display_name = display_name
                    aws_cred_.name = name
                    aws_cred_.owner = CREDENTIALS_MANAGER_USER
                    aws_cred_.role = aws_cred.role
                    lifespan_ = timedelta(hours=key_lifespan)
                    aws_cred_.lifespan = lifespan_.total_seconds()
                    share = ShareImpl()
                    share.user = sub
                    share.permissions = [Permission.VIEWER, Permission.DELETER]
                    aws_cred_.shares = [share]
                else:
                    aws_cred_.from_dict(aws_cred_old)
                aws_cred_.extend()
                if (id_ := await mongo_client.upsert_admin(aws_cred_, MONGODB_CREDENTIALS_COLLECTION, 
                                                           mongoattributes={'name': name})) is None:
                    raise ValueError('Failed to create managed credentials object in the database')
                aws_cred_.id = id_
            except ClientError as ce:
                raise ValueError(str(ce)) from ce
            return aws_cred_
    


async def _get_aws_credentials(request: web.Request) -> web.Response:
    obj = await mongoservicelib.get_desktop_object(request, MONGODB_CREDENTIALS_COLLECTION, type_=AWSCredentials)
    if obj is not None:
        sub = request.headers.get(SUB, NONE_USER)
        context = PermissionContext[AWSCredentials](sub)
        perms = await obj.get_permissions(context)
        attr_perms = await obj.get_all_attribute_permissions(context)
        obj_dict = obj.to_dict()
        return await response.get(request, obj_dict, permissions=perms, attribute_permissions=attr_perms)
    else:
        return response.status_not_found()


class _BadRequestException(Exception):
    pass


def main() -> None:
    config = init_cmd_line(description='a service for managing laboratory/user credentials',
                           default_port=8080)
    start(package_name='heaserver-keychain', db=aws.S3WithMongoManager,
          wstl_builder_factory=builder_factory(__package__),
          cleanup_ctx=[publisher_cleanup_context_factory(config),
                       scheduled_cleanup_ctx(coro=_delete_managed_coro, delay=3600)],
          config=config)
